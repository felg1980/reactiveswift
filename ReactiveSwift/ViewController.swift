//
//  ViewController.swift
//  ReactiveSwift
//
//  Created by Fredy Leon on 02/06/2020.
//  Copyright © 2020 Fredy Leon. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet weak var searchCityName: UITextField!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        style()
        
        ApiController.shared.currentWeather(city: "RxSwift")
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in
                self.tempLabel.text = "\(data.temperature)° C"
                self.iconLabel.text = data.icon
                self.humidityLabel.text = "\(data.humidity)%"
                self.cityNameLabel.text = data.cityName
            })
            .disposed(by: bag)
        
        
        searchCityName.rx.text
        .filter {
            ($0 ?? "").count > 0
        }
        .flatMap { text in
            return ApiController.shared.currentWeather(city: text ?? "Error")
         //Si hay un error no elimina el observable, y permite que la app sigu trabajando
        .catchErrorJustReturn(ApiController.Weather.empty)
        }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { data in
           self.tempLabel.text = "\(data.temperature)° C"
           self.iconLabel.text = data.icon
           self.humidityLabel.text = "\(data.humidity)%"
           self.cityNameLabel.text = data.cityName
        })
        .disposed(by:bag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
    }

    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()

      Appearance.applyBottomLine(to: searchCityName)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
    }

    override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
    }

    // MARK: - Style

    private func style() {
      view.backgroundColor = UIColor.aztec
      searchCityName.textColor = UIColor.ufoGreen
      tempLabel.textColor = UIColor.cream
      humidityLabel.textColor = UIColor.cream
      iconLabel.textColor = UIColor.cream
      cityNameLabel.textColor = UIColor.cream
    }

   
}

